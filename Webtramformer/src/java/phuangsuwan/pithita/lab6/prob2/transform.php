<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $catalog_xml = new DOMDocument;
        $catalog_xml->load('catalog.xml');
        $catalog_xsl = new DOMDocument;
        $catalog_xsl->load('catalog.xsl');
        
        $tran = new XSLTProcessor;
        $tran->importStyleSheet($catalog_xsl);
        echo $tran->transformToXML($catalog_xml);
        ?>
    </body>
</html>