<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
      <html><head><title>Catalog Transformation Result</title></head>
      <body>
          <h1>My CD Collection</h1>
          <table border="1">
          
              <tr bgcolor="fff955">
                  <th>CD Collection</th>
              </tr>
              <tr>
                  <xsl:for-each select="catalog/cd">
                      <tr bgcolor="e6e7f8">
                          <td>
                              <xsl:value-of select="title"/>
                          </td>
                      </tr>
                      
                  </xsl:for-each>
              </tr>
          </table>
      </body>
      </html>
        
    </xsl:template>

</xsl:stylesheet>
