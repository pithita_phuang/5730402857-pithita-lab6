package phuangsuwan.pithita.lab6.prob1;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static javax.ws.rs.client.Entity.xml;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

/**
 *
 * @author X64V2.0
 */
@WebServlet(name = "Tranformer", urlPatterns = {"/Tranformer"})
public class Tranformer extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<style>"
                    + "th {background-color: yellow;} "
                    + "td {background-color: LightGrey;} "
                    + "</style>");
            out.println("<title>Catalog Transformation</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Catalog Transformation Result </h1>");
            String xmlFile = request.getParameter("xml");
            String xslFile = request.getParameter("xsl");
            
            try {
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                docFactory.setNamespaceAware(true);
                DocumentBuilder parser = docFactory.newDocumentBuilder();
                
                Document doc = parser.parse(xmlFile);
                Document xslt = parser.parse(xslFile);
                Document result = transformXML(doc, xslt);
               
               // out.println(doc.getDocumentElement().getTextContent());
                
                LSSerializer serializer = ((DOMImplementationLS) doc.getImplementation()).createLSSerializer();
                out.println(serializer.writeToString(result));
                /* NodeList nList = doc.getElementsByTagName("cd");
                
                out.println("<h2>My CD Collection</h2>");
                out.println("<table>");
                out.println("<tr>");
                out.println("<th>CD Title</th");
                out.println("</tr>");
                
                for (int temp = 0; temp < nList.getLength(); temp++) {

		Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;
                out.println("<tr>");
                out.println("<td>"+eElement.getElementsByTagName("title").item(0).getTextContent()+"</td>");
                out.println("</tr>");
                    }
                }*/
                
                
            
            
        } catch (Exception ex) {
            System.err.println("There's an error.");
        }
             
            /*out.println("An input XML file is "+xmlFile+
                        " An input XSL file is " + xslFile);
            */
            
            
            
           
            out.println("</body>");
            out.println("</html>");
        }
    }
    public  Document transformXML(Document xml, Document xslt) throws TransformerException, ParserConfigurationException, FactoryConfigurationError {

        Source xmlSource = new DOMSource(xml);
        Source xsltSource = new DOMSource(xslt);
        DOMResult result = new DOMResult();

        // the factory pattern supports different XSLT processors
        TransformerFactory transFact = TransformerFactory.newInstance();
        Transformer trans = transFact.newTransformer(xsltSource);
        trans.transform(xmlSource, result);

        Document resultDoc = (Document) result.getNode();

        return resultDoc;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
   
}
